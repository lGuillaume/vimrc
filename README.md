# 👷 Guillaume's .vimrc files

My personal configuration files for Vim. Plugins are managed with vim-plug, a
minimalist Vim plugin manager.

Once you've cloned this repository, don't forget to run the following:

```sh
cd ~
ln -vs ~/.vim/vimrc ~/.vimrc
sudo update-alternatives --set editor /usr/bin/vim.basic
```

Then in a vim shell:

```vim
:PlugInstall
```

Enjoy!
