" Plugin manager
" https://github.com/junegunn/vim-plug
call plug#begin()
Plug 'ntk148v/vim-horizon' " Horizon theme
call plug#end()

" Horizon Theme configuration - BEGIN
set termguicolors
"colorscheme horizon
let g:lightline = {'colorscheme' : 'horizon'}
" Horizon Theme configuration - END

" My Custom config - BEGIN
set paste
set colorcolumn=80,120
